namespace :syllabus do
  desc "import data from json file"
  task :import, ['action'] => :environment do |task, args|
    Lecture.where(year: 2020).destroy_all if args[:action] == 'destroy'

    raw_data = JSON.parse(File.open("syllabus.json","r").read)
    raw_data.each do|row|
      data = Lecture.where(:title => row['title']['ja']).where(:year => 2020).limit(1).first()
      unless data.present? then
        data = Lecture.new
        data.uuid = SecureRandom.uuid
      end
      data.year = 2020
      data.target_year = row['target_year']
      data.quarter = row['quarter']
      data.title = row['title']['ja']
      data.title.chomp! if data.title.present?
      data.teachers = []
      row['teachers'].each do |teacher|
        exists_teacher = Teacher.find_by_name(teacher)
        if exists_teacher.present? then
          data.teachers.append(exists_teacher.uuid)
        else
          new_data = Teacher.new
          new_data.uuid = SecureRandom.uuid
          new_data.name = teacher

          data.teachers.append(new_data.uuid) if new_data.save
        end
      end

      data.week = row['week']
      data.time = row['time']
      data.course = row['course']
      data.room = if row['room'].present?
                    row['room']
                  else
                    ''
                  end
      begin
        data.save
      rescue
        next
      end

      p data
    end
  end
end
