FROM ruby:2.6.5-alpine

# ADD . ./
WORKDIR /tmp

RUN apk --update --no-cache add libxml2-dev libxslt-dev libstdc++ tzdata ca-certificates bash nodejs yarn \
    shadow sudo busybox-suid tzdata alpine-sdk libxml2-dev curl-dev postgresql-dev file file-dev vim

ADD ./ /usr/src/app
WORKDIR /usr/src/app

ADD Gemfile Gemfile.lock ./
RUN gem install bundler --no-document && \
    bundle update --bundler && \
    bundle install