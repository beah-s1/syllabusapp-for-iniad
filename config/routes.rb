Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  get '/' => redirect('/lectures')
  get '/lectures', to: 'syllabus#index'
  post '/lectures', to: 'syllabus#index'
  get '/lectures/:id', to: 'syllabus#show'
  post '/lectures/:uuid/check', to: 'syllabus#check_lecture'
  get '/my-lectures', to: 'syllabus#my_lecture'
  post '/my-lectures', to: 'syllabus#my_lecture'
  get  '/sync-toyo-app', to: 'syllabus#toyo_app_login'
  post '/sync-toyo-app', to: 'syllabus#toyo_app_sync'

  scope :auth do
    get '/sign_in', to: 'auth#login'
    get '/callback', to: 'auth#callback'
    get '/sign_out', to: 'auth#logout'
  end

  scope :teachers do
    get '/', to: 'teacher#index'
    get '/:id', to: 'teacher#show'
  end

  get 'about', to: 'application#about'
  get 'health', to: 'application#health'

  scope :api do
    get 'keygen', to: 'api#generate_key'

    scope 'my-lectures' do
      get '/', to: 'api#my_lectures'
    end

    scope 'lectures' do
      get '/', to: 'api#lectures'
      get '/:uuid', to: 'api#lecture'
    end

    scope 'teachers' do
      get '/', to: 'api#teachers'
      get '/:uuid', to: 'api#teacher'
    end
  end
end
