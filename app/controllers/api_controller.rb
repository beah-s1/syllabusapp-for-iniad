require 'devise'
require 'digest/sha2'

class ApiController < ApplicationController
  include Devise::Controllers::Helpers
  before_action :verify_key, :except => [:generate_key]

  def verify_key
    begin
      key = request.headers['Authorization'].split(' ')[1]
      user = ApiKey.find_by_hashed_key(Digest::SHA256.hexdigest(key))

      raise unless user.present? or user.revoked = true or user.expire_at < DateTime.now

    rescue
      render json:{"status" => "error", "description" => "invalid api key"}, status: 403
      return
    end
  end

  def search_user(key)
    return User.find_by_id(ApiKey.find_by_hashed_key(Digest::SHA256.hexdigest(key)).uid)
  end

  def generate_key
    unless signed_in? then
      session['current_access'] = request.fullpath
      redirect_to sign_in_path
      return
    end

    begin
      api_key = ApiKey.new
      api_key.uid = current_user.id

      key = SecureRandom.alphanumeric(32)
      api_key.hashed_key = Digest::SHA256.hexdigest(key)

      api_key.save!
    rescue
      retry
    end

    render json:{"status" => "OK", "api_key" => key}
  end

  def my_lectures
    user = search_user(request.headers['Authorization'].split(" ")[1])

    lectures = Lecture.where(:uuid => user.registered_lectures)
    render json:{"status" => "OK", "lectures" => lectures}
  end

  def lectures
    lectures = Lecture.all
    render json:{"status" => "OK", "lectures" => lectures}
  end

  def lecture
    unless params['uuid'].present?
      render json:{"status" => "error", "description" => "uuid must be specified"}, status: 400
      return
    end

    lecture = Lecture.find_by_uuid(params['uuid'])
    if lecture.present? then
      render json:{'status' => "OK", "lecture" => lecture}
    else
      render json:{'status' => 'error', 'description' => 'not found'}, status:404
    end
  end

  def teachers
    teachers = Teacher.all
    render json:{'status' => 'OK', 'teachers' => teachers}
  end

  def teacher
    teacher = Teacher.find_by_uuid(params['uuis'])
    unless teacher.present? then
      render json:{'status' => 'error', 'description' => 'not found'}, status:404
      return
    end

    render json:{'status' => 'OK', 'teacher' => teacher}
  end
end
