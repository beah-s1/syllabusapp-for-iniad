require 'net/http'
require 'net/https'
require 'uri'
require 'devise'

class AuthController < ApplicationController
  include Devise::Controllers::SignInOut
  def login
    # Googleにリダイレクト
    redirect_to "https://accounts.google.com/o/oauth2/v2/auth?client_id=#{ENV['OIDC_CLIENT_ID']}&response_type=code&scope=openid%20email%20profile&redirect_uri=#{ENV['HOST_SCHEMA']}://#{ENV['HOST_DOMAIN']}/auth/callback&hd=#{ENV['ALLOW_DOMAIN']}"
  end

  def callback
    # コールバック
    auth_code = params['code']
    unless auth_code.present?
      render plain: 'invalid code'
      return
    end

    uri = URI.parse("https://oauth2.googleapis.com/token")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    parameter = {
        'code' => auth_code,
        'grant_type' => "authorization_code",
        'client_id' => ENV['OIDC_CLIENT_ID'],
        'client_secret' => ENV['OIDC_CLIENT_SECRET'],
        'redirect_uri' => "#{ENV['HOST_SCHEMA']}://#{ENV['HOST_DOMAIN']}/auth/callback",
    }
    response = http.post(uri.path, parameter.to_json, {'Content-type' => 'x-www-form-urlencoded'})
    token_object = JSON.parse(response.body)

    access_token = token_object['access_token']
    id_token = token_object['id_token']
    url = URI.parse("https://www.googleapis.com/oauth2/v1/tokeninfo")
    url.query = URI.encode_www_form({
                                        id_token: id_token
                                    })
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    verify_response = JSON.parse(http.get(url).body)
    unless verify_response['issued_to'] == ENV['OIDC_CLIENT_ID'] or verify_response['issuer'] == ENV['OIDC_ISSUER'] then
      render plain: 'could not verify id_token'
      return
    end

    url = URI.parse('https://openidconnect.googleapis.com/v1/userinfo')
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true

    userinfo_response = http.get(url.path, {'Authorization' => "Bearer #{access_token}"})

    userinfo = JSON.parse(userinfo_response.body)
    if userinfo['hd'] != ENV['ALLOW_DOMAIN'] then
      render plain: "Service available only for G Suite user hosted in #{ENV['ALLOW_DOMAIN']}"
      return
    end

    # ユーザー認証・登録処理
    user = User.find_by_id(userinfo['sub'])
    unless user.present? then
      user = User.new
      user.id = userinfo['sub']
      user.email = userinfo['email']
      user.password = SecureRandom.alphanumeric(32)
      user.name = userinfo['name']

      user.save()
    end

    current_access = session['current_access']


    sign_in user
    if current_access.present? then
      redirect_to current_access
    else
      redirect_to '/'
    end

    return
  end

  def logout
    session['current_access'] = request.referer
    sign_out

    if session['current_access'].present?
      redirect_to session['current_access']
    else
      redirect_to "/"
    end

    return
  end
end
