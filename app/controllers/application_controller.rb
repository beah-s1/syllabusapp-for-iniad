class ApplicationController < ActionController::Base
  def about
    render 'about/index'
  end

  def health
    begin
      db_response = ActiveRecord::Migrator.current_version
    rescue ActiveRecordError
      render plain:"Error",status:503
      return
    end

    render json:{"status" => "OK", "db_response" => db_response},status:200
  end
end
