require 'devise'
require 'uri'
require 'net/http'
require 'net/https'

class SyllabusController < ApplicationController
  include Devise::Controllers::Helpers
  protect_from_forgery :only => [:check_lecture]

  def index
    @data = Lecture.all

    @data = @data.where(year: params[:year]) if params[:year].present?
    @data = @data.where(course: params[:course]) if params[:course].present?

    if params[:teacher].present? then
      target_teacher = Teacher.where('name ILIKE ?', "%#{params[:teacher]}%")[0]

      @data = @data.where('teachers @> ARRAY[?]::varchar[]', [target_teacher.uuid]) if target_teacher.present?
    end

    @data = @data.where('target_year @> ARRAY[?]::integer[]', [params[:target_year]]) if params[:target_year].present?
    @data = @data.where("title ILIKE ?", "%#{params[:title]}%") if params[:title].present?

    if params[:quarter].present? then
      @data = if params[:quarter] == "spring" then
                @data.where('quarter @> ARRAY[?]::integer[]', [1,2])
              elsif params[:quarter] == "fall" then
                @data.where('quarter @> ARRAY[?]::integer[]', [3,4])
              else
                @data.where('quarter @> ARRAY[?]::integer[]', [params[:quarter]])
              end
    end

    if params[:include_past_grade] != "on" and params[:target_year].present? then
      @data = @data.where('target_year[1] = ?', params[:target_year])
    end

    respond_to do |format|
      format.html
      format.json {render :json => @data}
    end
  end

  def show
    @data = Lecture.find_by_uuid(params[:id])
    @time = {
        0 => "月曜",
        1 => "火曜",
        2 => "水曜",
        3 => "木曜",
        4 => "金曜",
        5 => "土曜",
        6 => "日曜",
        7 => "集中"
    }[@data.week]

    @time += " / "

    @time += {
        1 => "1限",
        2 => "2限",
        3 => "3限",
        4 => "4限",
        5 => "5限",
        6 => "6限",
        7 => "7限",
        8 => "集中"
    }[@data.time]

    @quarter = if @data.quarter == [1,2] then
                "春学期"
              elsif @data.quarter == [3,4] then
                "秋学期"
              elsif @data.quarter == [0] then
                "集中"
              else
                "#{@data.quarter[0]}Q"
              end

    @available_grade = []
    @data.target_year.each do|grade|
      @available_grade.append("#{grade}年")
    end

    @available_grade = @available_grade.join(",")

    teachers = []
    @data.teachers.each do|teacher|
      teacher_row = Teacher.find_by_uuid(teacher)
      teachers.append(teacher_row.name) if teacher_row.present?
    end

    @teachers = teachers.join(', ')

    @room = if @data.room.present?
              @data.room
            else
              '情報なし'
            end


    respond_to do |format|
      format.html
      format.json {render :json => @data}
    end
  end

  def my_lecture
    unless signed_in?
      session['current_access'] = request.fullpath
      redirect_to sign_in_path
      return
    end

    @data = Lecture.where(:uuid => current_user.registered_lectures)

    @data = @data.where(year: params[:year]) if params[:year].present?
    @data = @data.where(course: params[:course]) if params[:course].present?

    if params[:teacher].present? then
      target_teacher = Teacher.where('name ILIKE ?', "%#{params[:teacher]}%")[0]

      @data = @data.where('teachers @> ARRAY[?]::varchar[]', [target_teacher.uuid]) if target_teacher.present?
    end

    @data = @data.where('target_year @> ARRAY[?]::integer[]', [params[:target_year]]) if params[:target_year].present?
    @data = @data.where("title ILIKE ?", "%#{params[:title]}%") if params[:title].present?

    if params[:quarter].present? then
      @data = if params[:quarter] == "spring" then
                @data.where('quarter @> ARRAY[?]::integer[]', [1,2])
              elsif params[:quarter] == "fall" then
                @data.where('quarter @> ARRAY[?]::integer[]', [3,4])
              else
                @data.where('quarter @> ARRAY[?]::integer[]', [params[:quarter]])
              end
    end

    if params[:include_past_grade] != "on" and params[:target_year].present? then
      @data = @data.where('target_year[1] = ?', params[:target_year])
    end

    respond_to do |format|
      format.html
      format.json {render :json => @data}
    end
  end

  def check_lecture
    unless signed_in? then
      session['current_access'] = request.fullpath
      redirect_to sign_in_path
      return
    end

    lecture = Lecture.find_by_uuid(params[:uuid])
    unless lecture.present?
      redirect_to '/'
      return
    end

    user = User.find_by_id(current_user.id)
    if params['is_register'] == 'on' then
      user.registered_lectures.append(lecture.uuid)
    else
      user.registered_lectures.select! {|a| a != lecture.uuid }
    end

    user.registered_lectures.uniq!
    user.save()

    redirect_back(fallback_location: '/')
  end

  def toyo_app_login
    unless signed_in?
      session['current_access'] = request.fullpath
      redirect_to sign_in_path
      return
    end
  end

  def toyo_app_sync
    unless signed_in?
      session['current_access'] = request.fullpath
      redirect_to sign_in_path
      return
    end
    # KnowledgePortal上での東洋大学の識別子
    toyo_api_code = 'Zk3nT7Qz6BST49bsjNd9DE8xJs2yNdptlDe20csjvJxumyME'

    unless current_user.toyo_api_key.present? then
      uri = URI.parse("https://light-api.portal.ac/auth/login")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true

      req = Net::HTTP::Post.new(uri.path)
      req['x-university-token'] = toyo_api_code
      req['content-type'] = 'application/json'

      data = {
          'instance_id': 'eEA6c3ctKjc',
          'user_id': params['toyo_id'],
          'password': params['toyo_pw']
      }.to_json

      req.body = data
      response = http.request(req)
      result = JSON.parse(response.body)

      if result['is_successful'] = false then
        render plain:'ログインに失敗しました'
        return
      end

      api_key = result['result']['token']
      user = User.find_by_id(current_user.id)
      user.toyo_api_key = api_key
      user.save()
    else
      api_key = current_user.toyo_api_key
    end

    uri = URI.parse("https://light-api.portal.ac/class-schedule/my-schedule")
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true

    req = Net::HTTP::Get.new(uri.path)
    req['x-university-token'] = toyo_api_code
    req['x-login-token'] = api_key

    response = http.request(req)
    result = JSON.parse(response.body)

    unless result['is_successful'] then
      render plain: '情報の取得に失敗しました'
      return
    end

    l_codes = []
    result['result']['items'].each do|reg_lecture|
      lecture = Lecture.where("REPLACE(REPLACE(title, ' ', ''),'　', '') ILIKE ?", "%#{reg_lecture['class_name']}%")
                .where(:week => reg_lecture['day_of_week'] - 1)
                .where(:time => reg_lecture['time_number'])
                .first()
      next unless lecture.present?

      l_codes.append(lecture.uuid)
    end

    user = User.find_by_id(current_user.id)
    user.registered_lectures = l_codes
    user.save()

    redirect_to my_lectures_path
  end
end
