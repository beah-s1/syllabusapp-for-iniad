class TeacherController < ApplicationController
  def index
    @data = Teacher.all
    respond_to do |format|
      format.json {render :json => @data}
    end
  end

  def show
    @data = Teacher.find_by_uuid(params['id'])
    respond_to do |format|
      format.json {render :json => @data}
    end
  end
end
