# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#
Course.create(id: 0, name: "共通科目")
Course.create(id: 1, name: "1年生")
Course.create(id: 2, name: "エンジニアリング")
Course.create(id: 3, name: "デザイン")
Course.create(id: 4, name: "ビジネス")
Course.create(id: 5, name: "シビル")
Course.create(id: 6, name: "未所属")