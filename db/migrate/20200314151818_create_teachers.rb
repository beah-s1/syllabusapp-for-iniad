class CreateTeachers < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers, id: false do |t|
      t.string :uuid, primary_key: true, unique: true, default: '', null: false
      t.string :name, null: false, default: ''
      t.timestamps
    end
  end
end
