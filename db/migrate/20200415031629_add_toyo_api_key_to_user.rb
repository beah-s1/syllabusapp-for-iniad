class AddToyoApiKeyToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :toyo_api_key, :string
  end
end
