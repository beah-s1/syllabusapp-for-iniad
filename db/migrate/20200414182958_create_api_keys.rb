class CreateApiKeys < ActiveRecord::Migration[6.0]
  def change
    create_table :api_keys do |t|
      t.string :uid, null: false, default: ''
      t.string :hashed_key, null: false, unique: true, default: ''
      t.boolean :revoked, null: false, default: false
      t.datetime :expire_at, null: false, default: DateTime.new(0)
      t.timestamps
    end

    add_foreign_key :api_keys, :users, column: :uid
  end
end
