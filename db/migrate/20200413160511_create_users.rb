class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users, id: false do |t|
      t.string :id, unique: true, primary_key: true, null: false, default: ''
      t.string :role, null: false, default: 'user'
      t.string :registered_lectures, array: true, default: []
      t.timestamps
    end
  end
end
