class CreateCourses < ActiveRecord::Migration[6.0]
  def change
    create_table :courses do |t|
      t.string :name, null: false, unique: true
      t.timestamps
    end

    add_foreign_key :lectures, :courses, column: "course"
  end
end
