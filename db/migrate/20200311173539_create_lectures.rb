class CreateLectures < ActiveRecord::Migration[6.0]
  def change
    create_table :lectures, id: false do |t|
      t.string :uuid, null: false, unique: true, primary_key: true, default: ""
      t.integer :quarter, null: false, array: true, default: []
      t.string :title, null: false, default: ""
      t.string :teachers, default: [], array: true
      t.integer :week, null: false, default: 0
      t.integer :time, null: false, default: 0
      t.integer :target_year, null: false, default: [], array: true
      t.integer :year, null: false
      t.integer :course, null: false, default: 0
      t.string :room

      t.timestamps
    end
  end
end
