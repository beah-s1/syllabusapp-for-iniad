# README

## REST API仕様
このアプリでは、JSON APIを通して情報を取得することができる。
ここでは、オリジナルバージョンにおける仕様を記載する（フォークされたプロジェクトでは、この仕様に準拠しない場合がある）
ベースURLは、デプロイ先のサーバーによって異なる。
なお、`HTTP/1.1`で取得のこと

### 非認証系API
APIキーが不要なAPI
時間割情報と、教員情報を取得できる。

#### 科目情報API `GET /lectures/[:uuid].json`
指定したUUIDに該当する時間割情報を取得する。

レスポンス例
```
{
    "uuid": "2a955b1f-d6c3-4089-ab46-2c5b239a99b1",
    "quarter": [
        3,
        4
    ],
    "title": "井上円了と日本近代思想 日本語①",
    "teachers": [
        "feef02d9-8c8f-4805-bcaf-0ae61528fc0a"
    ],
    "week": 0,
    "time": 3,
    "target_year": [
        1,
        2,
        3,
        4
    ],
    "year": 2020,
    "course": 0,
    "room": "ＩＮＩＡＤ　ホール",
    "created_at": "2020-04-14T04:36:42.755Z",
    "updated_at": "2020-04-14T04:36:42.755Z"
}
```

パラメーター
- `uuid:string`：科目固有のID、システム側で自動生成する。科目・クラスごとにユニークであることが保証されているが、データベースの入れ替えなどで変更になる可能性がある。
- `quater:[integer]`：開講クォーター、複数ある場合は、セメスタ開講を示し、クォーターで完結する授業の場合は別オブジェクトが生成されている。
- `title:string`：授業名
- `teachers:[string]`：担当教員のUUIDを列挙する、UUIDと教員名の対応付けについては教員情報APIを使用のこと。
- `week:integer`：開講曜日、月曜日→0 火曜日→1 水曜→2 木曜→3 金曜→4 土曜→5 日曜→6 集合→7 とする。
- `time:integer`：開講時限
- `target_year:[integer]`：受講可能学年、ただし原級留置やコース未配属などの事由により例外が生じる可能性がある。
- `year:integer`：年度、西暦で示す
- `course:integer`：コース、命名規則は以下の通り
    1. 共通、コースなどの区別はなし
    2. 1年生必修、CS概論やLS/RW演習など
    3. エンジニアリングコース
    4. デザインコース
    5. ビジネスコース
    6. シビルシステムコース
    7. 未配属
- `room:string`：授業が行われる教室を自然言語で表記する。部屋名は学務システムから抽出したものに準拠することから、全角文字で表現されていることがある。
- `created_at:datetime`：データの生成日時
- `updated_at:datetime`：データの最終更新日時

#### 科目一覧API `GET /lectures.json`
システムに登録されている科目を一覧で取得する。
細かい仕様については、科目情報APIを参照のこと。
（科目情報の配列として返却される）

レスポンス例
```
[
    {
        "uuid": "2a955b1f-d6c3-4089-ab46-2c5b239a99b1",
        "quarter": [
            3,
            4
        ],
        "title": "井上円了と日本近代思想 日本語①",
        "teachers": [
            "feef02d9-8c8f-4805-bcaf-0ae61528fc0a"
        ],
        "week": 0,
        "time": 3,
        "target_year": [
            1,
            2,
            3,
            4
        ],
        "year": 2020,
        "course": 0,
        "room": "ＩＮＩＡＤ　ホール",
        "created_at": "2020-04-14T04:36:42.755Z",
        "updated_at": "2020-04-14T04:36:42.755Z"
    },
    {
    ...
    }
]
```

#### 教員情報API `GET /teachers/[:uuid].json`
指定したUUIDに対応する教員の氏名を取得する。

レスポンス例
```
{
    "uuid": "feef02d9-8c8f-4805-bcaf-0ae61528fc0a",
    "name": "シュルツァ　ライナ",
    "created_at": "2020-04-14T04:36:42.751Z",
    "updated_at": "2020-04-14T04:36:42.751Z"
}
```

パラメーター
- `uuid:string`：教員のUUID、教員データベースにおいてユニークであることが保証されているが、データベースの入れ替えなどで変更になる可能性がある。
- `name:string`：教員の氏名、学務システム及びシラバスデータベースから抽出したものに準拠することから、全角文字で表現されていることがある。
- `created_at:datetime`：データの生成日時
- `updated_at:datetime`：データの最終更新日時

#### 教員一覧API `GET /teachers.json`
教員氏名の一覧を取得する。
細かい仕様については、教員情報APIを参照のこと。
（教員情報の配列として返却される）

レスポンス例
```
[
    {
        "uuid": "feef02d9-8c8f-4805-bcaf-0ae61528fc0a",
        "name": "シュルツァ　ライナ",
        "created_at": "2020-04-14T04:36:42.751Z",
        "updated_at": "2020-04-14T04:36:42.751Z"
    },
    {
        "uuid": "6a9470eb-2dd8-458b-89c3-0877e4d2d883",
        "name": "松原　俊文",
        "created_at": "2020-04-14T04:36:42.791Z",
        "updated_at": "2020-04-14T04:36:42.791Z"
    },
    {
        "uuid": "aa021cde-8e15-4b6a-919b-674457fcb52f",
        "name": "河井　理穂子",
        "created_at": "2020-04-14T04:36:42.806Z",
        "updated_at": "2020-04-14T04:36:42.806Z"
    },
    {
        "uuid": "fc0674cb-4db0-46de-9b62-9769ae48cf9f",
        "name": "益田　安良",
        "created_at": "2020-04-14T04:36:42.814Z",
        "updated_at": "2020-04-14T04:36:42.814Z"
    },
    {
        "uuid": "af132ad4-57ae-4a65-8863-afe2b3b131f4",
        "name": "水松　巳奈",
        "created_at": "2020-04-14T04:36:42.884Z",
        "updated_at": "2020-04-14T04:36:42.884Z"
    },
    {
        "uuid": "9e8beb78-74e1-4262-84f5-27551afdd4d0",
        "name": "RAUBER　Max　Laurent",
        "created_at": "2020-04-14T04:36:42.892Z",
        "updated_at": "2020-04-14T04:36:42.892Z"
    }
]
```

### 認証系API
認証が必要なAPI
APIキーは、ログインしているユーザーの操作によって発行される。

認証を行う際は、リクエストヘッダに`Authorization: Bearer APIキー`を含める必要がある。

#### APIキー発行API `GET /api/keygen`
APIキーを発行する。
現在はブラウザでのみ利用可（OAuthのような、ユーザーの同意を得てアプリケーション側からAPIキーをクレームする方法を検討中）

レスポンス例（APIキーはダミー）
```
{
    "status": "OK",
    "api_key": "U41nqD7n1x2onQ58C0oEBqj7Khfl7Eyk"
}
```

#### 履修メモAPI `GET /api/my-lectures`
ユーザーが履修メモに登録している科目の一覧が表示される。
その他、パラメーターなどの仕様については、非認証系APIの科目一覧APIと共通である。

#### 科目情報API `GET /api/lectures/:uuid`
非認証系API＞科目情報APIと共通

#### 科目一覧API `GET /api/lectures`
非認証系API＞科目一覧APIと共通

#### 教員情報API `GET /api/teachers/:uuid`
非認証系API＞教員情報APIと共通

#### 教員一覧API `GET /api/teachers`
非認証系API＞教員一覧APIと共通